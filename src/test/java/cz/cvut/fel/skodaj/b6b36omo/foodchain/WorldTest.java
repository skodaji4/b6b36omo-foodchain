/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.config.Configuration;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Foodstuff;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Payment;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Request;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Customer;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Farmer;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Vendor;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.security.CertificationAuthority;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing class for testing public API (class World)
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class WorldTest {
    
    
    public WorldTest()
    {
    }
    

    /**
     * Test of getTime method, of class World.
     */
    @Test
    public void testGetTime() {
        System.out.println("getTime");
        World instance = World.getInstance();
        LocalDateTime expResult = LocalDateTime.now();
        LocalDateTime result = instance.getTime();
        assertEquals(expResult, result);
    }
    /**
     * Test of createParty method, of class World.
     */
    @Test
    public void testCreateParty() throws Exception {
        System.out.println("createParty");
        String type = "VENDOR";
        String name = "TestVendor";
        World instance = World.getInstance();
        instance.createCertificationAuthority("Test Authority");
        Party expResult = new Vendor(name);
        Party result = instance.createParty(type, name);
        assertEquals(expResult.getName(), result.getName());
    }

    /**
     * Test of getCustomers method, of class World.
     */
    @Test
    public void testGetCustomers() throws NoCAAvailableException {
        System.out.println("getCustomers");
        World instance = World.getInstance();
        instance.createCertificationAuthority("Test Authority");
        Customer c = new Customer("Test Customer");
        instance.createParty("CUSTOMER", "Test Customer");
        List<Customer> expResult = new ArrayList<>();
        expResult.add(c);
        List<Customer> result = instance.getCustomers();
        assertEquals(expResult.get(0).getName(), expResult.get(0).getName());
    }

    /**
     * Test of getParties method, of class World.
     */
    @Test
    public void testGetParties() throws NoCAAvailableException {
        System.out.println("getParties");
        Farmer party = new Farmer("Test Party");
        World instance = World.getInstance();
        instance.createCertificationAuthority("Test Authority");
        instance.createParty("FARMER", "Test Party");
        List<Party> expResult = new ArrayList<>();
        expResult.add(party);
        List<Party> result = instance.getParties();
        assertEquals(expResult.get(0).getName(), result.get(0).getName());
    }
    
}
