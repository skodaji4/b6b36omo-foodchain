/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing channel for communication between parties
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Channel
{
    /**
     * List of joined parties
     */
    protected List<Party> parties;
    
    /**
     * Result of channel
     */
    protected String result;
    
    /**
     * Which type of party can join channel
     */
    protected PartyFactory.PartyType  type;
    
    /**
     * Creates new channel
     * @param result What is result of channel
     * @param type Which type of party can join channel
     */
    public Channel(String result, PartyFactory.PartyType  type)
    {
        this.result = result;
        this.parties = new ArrayList<>();
        this.type = type;
    }
    
    /**
     * Joins party to channel
     * @param p Party which joins channel
     */
    public void join(Party p)
    {
        if (this.parties.contains(p) == false)
        {
            this.parties.add(p);
        }
        
    }
    
    /**
     * Leaves party from channel
     * @param p Party which leaves channel
     */
    public void leave(Party p)
    {
        this.parties.remove(p);
    }
    
    /**
     * Gets number of joined parties
     * @return Number of joined parties
     */
    public int getCount()
    {
        return this.parties.size();
    }
    
    /**
     * Gets which type of party can join channel
     * @return Type of party which can join channel
     */
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
    
    
    /**
     * Checks, whether channel can handle request
     * @param req Request which should be handled
     * @return {@code TRUE} if channel can handle request,
     *         {@code FALSE} otherwise
     */
    public boolean canHandleRequest(Request req)
    {
        boolean reti = false;
        if ((req.getType() == this.getType() || req.getType() == PartyFactory.PartyType.ANY) && 
                (req.getGoal().getOutput() == null ? 
                this.result == null : 
                req.getGoal().getOutput().equals(this.result))&&
                (req.getDestination() == null || this.parties.contains(req.getDestination()))
                )
        {
            reti = true;
        }
        return reti;
    }
    
    /**
     * Handles request
     * @param req Request which will be handled by channel
     */
    public void handleRequest(Request req)
    {
        if (this.checkRequest(req))
        {
            boolean handled = false;
            Party handler = null;
            if (this.canHandleRequest(req))
            {     
                while (handled == false)            // Find any party which wants handle request
                {
                    for (Party p : this.parties)
                    {
                        if (p.canHandleRequest(req))
                        {
                            handler = p;
                            handled = true;
                            break;
                        }
                    }
                }

            }
            if (handler != null && handled == true)
            {
                handler.handleRequest(req);
            }
        }
    }
    
    /**
     * Gets result of channel
     * @return Result of channel
     */
    public String getResult()
    {
        return this.result;
    }

    /**
     * Checks, whether request can be done
     * @param req Request which should be checked
     * @return {@code TRUE} if request can be done,
     *         {@code FALSE} otherwise
     */
    private boolean checkRequest(Request req)
    {
        boolean reti = false;
        
        if (    req.getFoodstuff().getTransactions().size() < PartyFactory.partyOrder.length && (
                req.getFoodstuff().getTransactions().size() == 0 ||
                PartyFactory.partyOrder[req.getFoodstuff().getTransactions().size()] == req.getType()))
        {
            reti = true;
        }
        if (reti == false)
        {
            World.getInstance().addSecurityReport(new Report("Party " + req.getSender().getName() + " tries to do action '" + req.getGoal().getName() + "', which has been already done on '" + req.getFoodstuff().getName() + "'"));
        }
        return reti;
    }
    
}
