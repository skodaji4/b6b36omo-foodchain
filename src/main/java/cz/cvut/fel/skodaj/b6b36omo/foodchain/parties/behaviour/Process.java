/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import java.time.Duration;

/**
 * Class representing action which is done in processor
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Process extends Instruction
{
    /**
     * Output of processing
     */
    private String output;
    
   /**
     * Sets that this action can be finished only on processor
     */
    private PartyFactory.PartyType type;
    
    /**
     * Creates new action which is done in processor
     * @param output Output of process
     * @param time Time needed for process
     * @param cost Cost of processing
     */
    public Process(String output, Duration time, int cost)
    {
        super(cost, time, output);
        this.cost = cost;
        this.time = time;
        this.output = output;
        this.type = PartyFactory.PartyType.PROCESSOR;
    }
    
    /**
     * Gets type of party which can handle instruction
     * @return Party which can handle instruction
     */
    @Override
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
    
    /**
     * Gets instruction name - processing
     * @return {@code "Processing"}
     */
    @Override
    public String getName()
    {
        return "Processing";
    }    
}
