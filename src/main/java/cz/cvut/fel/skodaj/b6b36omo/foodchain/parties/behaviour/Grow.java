/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import java.time.Duration;

/**
 * Class representing growing process at farm
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Grow extends Instruction
{
    /**
     * Plant which will be grown on farm
     */
    private String plant;
    
    
    /**
     * Output of growing process
     */
    private String output;
    
    /**
     * Creates new growing process
     * @param plant Plant which will be grown
     * @param time Time needed for growing
     * @param output Output of growing process
     * @param cost Cost of growing
     */
    public Grow(String plant, Duration time, String output, int cost)
    {
        super(cost, time, output);
        this.plant = plant;
        this.time = time;
        this.output = output;
        this.cost = cost;
        this.type = PartyFactory.PartyType.FARMER;
    }
    
    /**
     * Gets plant of growing process
     * @return Plant of growing process
     */
    public String getPlant()
    {
        return this.plant;
    }
    
    /**
     * Gets instruction name - growing
     * @return {@code "Growing"}
     */
    @Override
    public String getName()
    {
        return "Growing";
    }
    
    /**
     * Gets type of party which can handle instruction
     * @return Party which can handle instruction
     */
    @Override
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
}
