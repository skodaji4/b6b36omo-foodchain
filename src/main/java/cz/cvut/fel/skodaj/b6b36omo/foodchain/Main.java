/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.config.*;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Foodstuff;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Customer;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import java.util.Random;


/**
 * Entry class of whole program
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Main
{
    /**
     * Entry point of program
     * @param args Arguments of program
     * @throws java.lang.Exception Exception when something bad happened
     */
    public static void main(String[] args) throws Exception
    {
        //Load configuration from object
        Configuration config = Configuration.getConfig();
        config.loadConfig(new SampleConfiguration());
        
        //Set up whole system from configuration
        World world = World.getInstance();
        world.setUpFromConfig();
        
        
        //Make all foodstuff (and generate reports)
        for (Foodstuff f : World.getInstance().getFoodstuff())
        {
            world.makeFoodstuff(
                    world.getCustomers().get(
                    (new Random()).nextInt(
                    world.getCustomers().size()
                    )
                    ), f);
            String path = World.getInstance().getConfig().getDefaultPath() + "F_REPORT_" + f.getName().toUpperCase().replace(" ", "-").replace(".", "")  + ".TXT";
            world.makeFoodstuffReport(f, path);
        }
        
        //Make parties reports
        for (Party p : world.getParties())
        {
            String path = World.getInstance().getConfig().getDefaultPath() + "P_REPORT_" + p.getName().toUpperCase().replace(" ", "-").replace(".", "") + ".TXT";
            world.makePartyReport(p, path);
        }
        
        //Generate security report
        world.generateSecurityReport(world.getConfig().getDefaultPath() + "SECURITY_REPORT.TXT");
    }
}
