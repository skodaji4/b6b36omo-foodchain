/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;

/**
 * Class representing payment between parties
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Payment
{
    /**
     * Sender of payment
     */
    private Party sender;
    
    /**
     * Receiver of payment
     */
    private Party receiver;
    
    /**
     * Amount of money
     */
    private int amount;
    
    /**
     * Creates new payment
     * @param sender Sender of payment
     * @param receiver Receiver of payment
     * @param amount Amount of money
     */
    public Payment(Party sender, Party receiver, int amount)
    {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
    }

    Payment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Gets sender of payment
     * @return Sender of payment
     */
    public Party getSender()
    {
        return this.sender;
    }
    
    /**
     * Gets receiver of payment
     * @return Receiver of payment
     */
    public Party getReceiver()
    {
        return this.receiver;
    }
    
    /**
     * Gets amount of money
     * @return Amount of money
     */
    public int getAmount()
    {
        return this.amount;
    }
}
