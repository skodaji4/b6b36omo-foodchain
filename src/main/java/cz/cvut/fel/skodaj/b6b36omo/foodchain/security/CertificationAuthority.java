/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.security;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import java.util.HashMap;
import java.util.Map;

/**
 * Class representing trusted certificate authority of world
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class CertificationAuthority
{
    /**
     * Unique identifier of certification authority
     */
    private int id;
    
    /**
     * Name of certification authority
     */
    private String name;
    
    /**
     * Database of issued certification for parties
     */
    private Map<Integer, Party> partyDB;
    
    /**
     * Creates new certification authority
     * @param name Name of certification authority
     * 
     */
    public CertificationAuthority(String name)
    {
        this.name = name;
        this.partyDB = new HashMap<>();
        this.id = World.getInstance().getIdentifier();
    }
        /**
     * Checks whether authority issued set certificate
     * @param cert Certificate which will be checked
     * @return {@code TRUE} if certificate has been issued by authority, 
     *         {@code FALSE} otherwise
     */
    public boolean checkIssuedBy(Certificate cert)
    {
        boolean reti = false;
        if (this.partyDB.containsKey(cert.getId()))
        {
            reti = true;
        }
        return reti;
    }
    
    /**
     * Checks whether certificate has been issued for party in certificate
     * @param cert Certificate which will be checked
     * @return {@code TRUE} if certificate has been issued for party in certificate,
     *         {@code FALSE} otherwise
     */
    public boolean checkIssuedFor(Certificate cert)
    {
        boolean reti = false;
        if (this.checkIssuedBy(cert))
        {
            Party p = this.partyDB.get(cert.getId());
            if (p.getId() == cert.getParty().getId())
            {
                reti = true;
            }
        }
        return reti;
    }
    
        /**
     * Checks whether certificate has been issued for party
     * @param cert Certificate which will be checked
     * @param p Party of which certificate ownership will be checked
     * @return {@code TRUE} if certificate has been issued for party,
     *         {@code FALSE} otherwise
     */
    public boolean checkIssuedFor(Certificate cert, Party p)
    {
        boolean reti = false;
        if (this.checkIssuedBy(cert))
        {
            if (p.getId() == cert.getParty().getId())
            {
                reti = true;
            }
        }
        return reti;
    }
    
    /**
     * Gets count of issued certificates
     * @return Count of issued certificates
     */
    public int getCertificatesCount()
    {
        return this.partyDB.size();
    }
    
    /**
     * Issue new certificate
     * @param p Party which asks for certificate
     * @return New certificate
     */
    public Certificate issueCertificate(Party p)
    {
        Certificate reti = new Certificate(this, p);
        this.partyDB.put(reti.getId(), p);
        return reti;
    }
}
