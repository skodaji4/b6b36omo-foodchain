/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Grow;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;

/**
 * Class representing farmers in food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Farmer extends AbstractParty
{
    
    /**
     * Creates new farmer
     * @param name Name of farmer
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *          If there is no available certification authority
     */
    public Farmer(String name) throws NoCAAvailableException
    {
        super(name, PartyFactory.PartyType.FARMER);
    }

    /**
     * Performs grow instruction
     * @param instruction Grow instruction which will be done
     */
    @Override
    protected void performInstruction(Instruction instruction)
    {
        if (instruction.getType() == this.type)
        {
            super.performInstruction(instruction);
            Grow g = (Grow)instruction;
            this.reports.add(new Report("Growing " + g.getPlant() + " to " + g.getOutput() + " (" + this.req.getFoodstuff().getName() + ") for " + g.getTime().toString()));
        }
        super.doNext();
    }
    
    
}
