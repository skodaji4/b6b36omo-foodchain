/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Channel;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Payment;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Request;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;

/**
 * Interface allowing communication with parties in food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public interface Party
{
    /**
     * Gets name of party
     * @return Name of party
     */
    public String getName();
    
    /**
     * Gets identifier of party
     * @return Identifier of party
     */
    public int getId();
    
    /**
     * Decision of joining channel
     * @param ch Channel which can be joined
     */
    public void joinChannel(Channel ch);
    
    /**
     * Gets type of party
     * @return Type of party
     */
    public PartyFactory.PartyType getType();
    
    /**
     * Decision if party handles request
     * @param req Request which should be handled
     * @return {@code TRUE} if party wants handle request,
     *         {@code FALSE} otherwise
     */    
    public boolean canHandleRequest(Request req);
    
    /**
     * Handles request
     * @param req Request which should be checked
     */
    public void handleRequest(Request req);
    
    /**
     * Gets one entry to hash
     * @return entry to hash
     */
    public int getSubHash();
    
    /**
     * Gets how much money party has
     * @return How much money party has
     */
    public int getMoney();
    
    /**
     * Charges party by price
     * @param value Price of which will be party charged
     */
    public void charge(int value);
    
    /**
     * Makes report of party
     * @return Report of party
     */
    public String makeReport();
    
    /**
     * Adds money to party
     * @param value Which amount will be added
     */
    public void income(int value);
    
    
    /**
     * Adds payment report to party
     * @param pay Payment which should be reported
     */
    public void addPaymentReport(Payment pay);

}
