/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import java.time.Duration;

/**
 * Class representing distribution
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Distribute extends Instruction
{
    /**
     * Speed of distribution
     */
    private int speed;
    
    /**
     * Distance of distribution
     */
    private int distance;
    
    
    /**
     * Sets that this action can be finished only at distribution
     */
    private PartyFactory.PartyType type;
    
    /**
     * Creates new distribution
     * @param distance Distance of distribution
     * @param speed Speed of distribution
     * @param cost Cost of distribution
     * @param output Output of distribution
     */
    public Distribute(int distance, int speed, int cost, String output)
    {
        super(cost,Duration.ofSeconds((int)Math.ceil((double)((double)distance / (double)speed))) , output);
        this.cost = cost;
        this.distance = distance;
        this.speed = speed;
        this.time = Duration.ofSeconds((int)Math.ceil((double)((double)distance / (double)speed)));
        this.output = output;
        this.type = PartyFactory.PartyType.DISTRIBUTOR;
    }
    
    /**
     * Gets distance of distribution
     * @return 
     */
    public int getDistance()
    {
        return this.distance;
    }
    
    /**
     * Gets speed of distribution
     * @return Speed of distribution
     */
    public int getSpeed()
    {
        return this.speed;
    }
    
    /**
     * Gets instruction name - distributing
     * @return {@code "Distributing"}
     */
    @Override
    public String getName()
    {
        return "Distributing";
    }
    
    /**
     * Gets type of party which can handle instruction
     * @return Party which can handle instruction
     */
    @Override
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
}
