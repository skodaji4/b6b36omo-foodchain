/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Payment;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Request;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Sell;

/**
 * Class representing vendor of foodstuff
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Vendor extends AbstractParty
{
    
    /**
     * Creates new vendor of foodstuff
     * @param name Name of new vendor
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *          If there is no available authority
     */
    public Vendor(String name) throws NoCAAvailableException
    {
        super(name, PartyFactory.PartyType.VENDOR);
    }
    
    /**
     * Performs sell instruction
     * @param instruction Sell instruction which will be done
     */
    @Override
    protected void performInstruction(Instruction instruction)
    {
        if (instruction.getType() == this.type)
        {
            super.performInstruction(instruction);
            Sell s = (Sell)instruction;
            this.reports.add(new Report(
            "Selling " + s.getOutput() + " for " + s.getPrice()
            ));                    
            
        }
        super.doNext();
    }
    
   /**
     * Handles request
     * @param req Request which should be handled
     */
    @Override
    public void handleRequest(Request req)
    {
        if (req.getType() == PartyFactory.PartyType.VENDOR)
        {
            Sell s = (Sell)req.getGoal();
            Payment pay = new Payment(req.getFoodstuff().getCustomer(), this, s.getPrice());
            World.getInstance().makePayment(pay);
        }        
        super.handleRequest(req);
        
    }
}
