/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Payment;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Request;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Distribute;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;

/**
 * Class representing distributor of foodstuff in food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Distributor extends AbstractParty
{
    /**
     * Creates new distributor of foodstuff in food chain
     * @param name Name of new distributor
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *          If there is no available certification authority
     */
    public Distributor(String name) throws NoCAAvailableException
    {
        super(name, PartyFactory.PartyType.DISTRIBUTOR);
    }
    
    /**
     * Performs distribute instruction
     * @param instruction Distribute instruction which will be done
     */
    @Override
    protected void performInstruction(Instruction instruction)
    {
        if (instruction.getType() == this.type)
        {
            super.performInstruction(instruction);
            Distribute d = (Distribute)instruction;
            this.reports.add(new Report("Distributing " + this.req.getFoodstuff().getName() + " from " + 
                    this.req.getFoodstuff().getLastTransaction().getParty().getName() + " to " + 
                    this.req.getFoodstuff().getCustomer().getName() + "(distance: " + 
                    d.getSpeed() + "; needed time: " + d.getTime() + ")"));
            
        }
        this.req.getFoodstuff().getCustomer().receiveFoodStuff(this.req.getFoodstuff(), this);
        super.doNext();
    }
    
    /**
     * Handles request
     * @param req Request which should be handled
     */
    @Override
    public void handleRequest(Request req)
    {
        if (req.getType() == PartyFactory.PartyType.DISTRIBUTOR)
        {
            Payment pay = new Payment(req.getFoodstuff().getCustomer(), this, req.getGoal().getCost());
            World.getInstance().makePayment(pay);
        }
        super.handleRequest(req);
    }
    
}
