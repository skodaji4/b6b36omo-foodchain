/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import java.time.Duration;

/**
 * Class representing storing foodstuff 
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Store  extends Instruction
{
    /**
     * Needed temperature
     */
    private double temperature;
    
    /**
     * Needed humidity
     */
    private double humidity;
    
    /**
     * Sets that this action can be finished only at warehouse
     */
    private PartyFactory.PartyType type;
    
    /**
     * Output of storing
     */
    private String output;
    
    /**
     * Creates new storing
     * @param time Time of storing
     * @param temperature Temperature of storing
     * @param humidity Humidity of storing
     * @param cost Cost of storing
     * @param output Output of storing
     */
    public Store(Duration time, double temperature, double humidity, int cost, String output)
    {        
        super(cost, time, output);
        this.cost = cost;
        this.time = time;
        this.output = output;
        this.temperature = temperature;
        this.humidity = humidity;
        this.type = PartyFactory.PartyType.WAREHOUSE;
    }
    

    /**
     * Gets temperature needed for storing
     * @return Temperature needed for storing
     */
    public double getTemperature()
    {
        return this.temperature;
    }

    /**
     * Gets humidity needed for storing
     * @return Humidity needed for storing
     */
    public double getHumidity()
    {
        return this.humidity;
    }

    /**
     * Gets instruction name - storing
     * @return {@code "Storing"}
     */
    @Override
    public String getName()
    {
        return "Distributing";
    }
    
    /**
     * Gets type of party which can handle instruction
     * @return Party which can handle instruction
     */
    @Override
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
}
