/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.config;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Foodstuff;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.FoodstuffBuilder;
import java.time.Duration;

/**
 * Class representing configuration of system
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public abstract class TemplateConfiguration
{
    /**
     * Array of names of available certification authorities
     */
    public String[] CAs = 
    {
        "TrustedPlatform Ltd.",
        "FancyCertificates Inc.",
        "CrazyCert Inc."
    };
    
    /**
     * Array of names and types of available parties
     * syntax:
     * {"A", "B"}
     *      -"A": Type of party
     *      -"B": Name of party
     */
    
    public String[][] parties = 
    {
        {"FARMER", "ExperimentalGrow Ltd."},
        {"FARMER", "Gentica GmbH"},
        {"FARMER", "Chernobyl EcoLab Inc."},
        {"FARMER", "InstantLife Ltd."},
        {"FARMER", "Multiplicator Inc."},
        {"PROCESSOR", "PoisonLab Inc."},
        {"PROCESSOR", "SimplySqueezer Inc."},
        {"PROCESSOR", "AsYouWish Ltd."},
        {"PROCESSOR", "TopSecretKnowHow Ltd."},
        {"PROCESSOR", "JustDoIt Inc."},
        {"WAREHOUSE", "FreezeTillDeath Inc."},
        {"WAREHOUSE", "StoreEverythingWithoutAsking Ltd."},
        {"WAREHOUSE", "PayInAdvanceHouse Inc."},
        {"WAREHOUSE", "Dump Ltd."},
        {"WAREHOUSE", "OrbitalWareHouse Inc."},
        {"VENDOR", "TotalSellout Ltd."},
        {"VENDOR", "ThinkLessBuyMore Inc."},
        {"VENDOR", "CheapJunk Inc."},
        {"VENDOR", "UltraHyperSuperMarket Inc."},
        {"VENDOR", "BuyInMasses Ltd."},
        {"DISTRIBUTOR", "EscortService Ltd."},
        {"DISTRIBUTOR", "FasterThanPost Inc."},
        {"DISTRIBUTOR", "Airship Transport Inc."},
        {"DISTRIBUTOR", "TotallyNotUsingSlaves Ltd."},
        {"DISTRIBUTOR", "WeDontHurry Ltd."},
        {"CUSTOMER", "Garbage Intl. Inc."},
        {"CUSTOMER", "NotFreshButGood Ltd."},
        {"CUSTOMER", "WeWantItNow Ltd."},
        {"CUSTOMER", "Contoso Inc."},
        {"CUSTOMER", "EatIT Ltd."}
    };
    
    /**
     * Probability of acceptance (in percents)
     */
    public int acceptanceProbability = 50;
    
    /**
     * List of foodstuff which will be made
     */
    public Foodstuff[] foodstuffs = 
    {
      (new Foodstuff(
        "CheapyFries", 
              (new FoodstuffBuilder())
              .grow("Potatoes", Duration.ofDays(130), "Potatoes", 300)
              .process("Cut potatoes", Duration.ofDays(2), 100)
              .store("Frozen fries", Duration.ofDays(61), -16, 13, 500)
              .sell("CheapyFries", Duration.ofDays(28), 2000, 100)
              .distribute("Thawed CheapyFries", 3000, 10, 50)
              .getInstructions()
        ) 
      ),
      (new Foodstuff(
        "Le Ketchup Exklusif",
              (new FoodstuffBuilder())
              .grow("Tomatoes", Duration.ofDays(260), "Tomatoes seeds", 500)
              .process("Mashed Tomatoes", Duration.ofDays(2), 200)
              .store("Low quality ketchup", Duration.ofDays(31), 35, 60, 300)
              .sell("Le Ketchup Exklusif", Duration.ofDays(63), 10150, 100)
              .distribute("Degraded Le Ketchup Exlusif", 10000, 100, 80)
              .getInstructions()
        )      
      ),
      (new Foodstuff(
        "BigNuts",
              (new FoodstuffBuilder())
              .grow("Arachis hypogaea", Duration.ofDays(365), "Peanuts", 2000)
              .process("Fresh salted peanuts", Duration.ofDays(7), 800)
              .store("Salted peanuts", Duration.ofDays(128), 16, 12, 500)
              .sell("BigNuts", Duration.ofDays(250), 10150, 200)
              .distribute("Broken BigNuts", 100000010, 12, 500)
              .getInstructions()
        )      
      ),
      (new Foodstuff(
        "MagicBeans",
              (new FoodstuffBuilder())
              .grow("Cheap Beans", Duration.ofDays(50), "Beans", 500)
              .process("Radioactive Beans", Duration.ofDays(12), 1200)
              .store("Lighting Beans", Duration.ofDays(20), 100, 0, 22500)
              .sell("MagicBeans", Duration.ofDays(10), 200000, 210)
              .distribute("MagicBeansPowder", 12045324, 501, 500)
              .getInstructions()
        )      
      ),
      (new Foodstuff(
        "SomethingInCan",
              (new FoodstuffBuilder())
              .grow("Cauliflower", Duration.ofDays(64), "Cauliflower", 456)
              .process("Mashed Cauliflower", Duration.ofDays(20), 452)
              .store("SomethingRawInCan", Duration.ofDays(345), 123, 435, 1234)
              .sell("SomethingInCan", Duration.ofDays(213), 453453, 123)
              .distribute("SomethingInDamagedCan", 12535, 45, 21)
              .getInstructions()
        )
      ),
      (new Foodstuff(
        "Peashooter",
              (new FoodstuffBuilder())
              .grow("Pea seeads", Duration.ofDays(53), "Pea", 453)
              .process("Magic pea", Duration.ofDays(89), 436)
              .store("SleepingPeashooter", Duration.ofDays(876), 43, 578, 3778)
              .sell("Peashooter", Duration.ofDays(23), 78967, 235)
              .distribute("Angry Peashooter", 78354, 213, 45)
              .getInstructions()
        )
      ),
      (new Foodstuff(
        "Overpriced popcorn",
              (new FoodstuffBuilder())
              .grow("Still edible corn", Duration.ofDays(53), "Corn", 453)
              .process("Raw popcorn", Duration.ofDays(89), 436)
              .store("Dry raw popcorn", Duration.ofDays(876), 43, 578, 3778)
              .sell("Overpriced popcorn", Duration.ofDays(23), 78967, 235)
              .distribute("Corn flour", 78354, 213, 45)
              .getInstructions()
        )
      )      
    };
    
    /**
     * Default path for reporting
     */
    public String defaultPath = "D:\\ownCloud\\foodchain_reports\\";
    
    /**
     * Defines, whether transaction reports should be made
     */
    public boolean makeTransactionReport = false;
    
    /**
     * Defines, whether there should be scammer vendor
     */
    public boolean scammerVendor = false;
    
}
