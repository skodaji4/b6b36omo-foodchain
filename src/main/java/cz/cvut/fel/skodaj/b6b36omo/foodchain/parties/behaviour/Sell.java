/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import java.time.Duration;

/**
 * Class representing selling of foodstuff
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Sell extends Instruction
{
    /**
     * Amount of income by selling
     */
    private int price;
    
    /**
     * Sets that this instruction can be handled only by vendor
     */
    private PartyFactory.PartyType type;
    
    /**
     * Output of selling
     */
    private String output;
    
    /**
     * Creates new selling instruction
     * @param output Output of selling
     * @param time Time needed for selling
     * @param price Price of foodstuff
     * @param cost Cost of selling
     */
    public Sell(String output, Duration time, int price, int cost)
    {
        super(cost, time, output);
        this.output = output;
        this.time = time;
        this.cost = cost;
        this.price = price;
        this.type = PartyFactory.PartyType.VENDOR;
    }
    
    
    /**
     * Gets price of foodstuff
     * @return Price of foodstuff 
     */
    public int getPrice()
    {
        return this.price;
    }
    
    /**
     * Gets instruction name - selling
     * @return {@code "Selling"}
     */
    @Override
    public String getName()
    {
        return "Selling";
    }
    
    /**
     * Gets type of party which can handle instruction
     * @return Party which can handle instruction
     */
    @Override
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
}
