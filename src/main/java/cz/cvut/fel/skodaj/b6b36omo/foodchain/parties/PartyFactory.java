/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NotSuchAPartyTypeExistsException;

/**
 * Class which creates parties in food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class PartyFactory
{
    /**
     * Enumeration of all possible parties of system
     */
    public enum PartyType
    {
        /**
         * Farmer of foodstuff
         */
        FARMER,
        
        /**
         * Processor of foodstuff
         */
        PROCESSOR,
        
        /**
         * Warehouse of foodstuff
         */
        WAREHOUSE,
        
        /**
         * Vendor of foodstuff
         */
        VENDOR,
        
        /**
         * Distributor of foodstuff
         */
        DISTRIBUTOR,
        
        /**
         * Customer of foodstuff
         */
        CUSTOMER,
        
        /**
         * Any type of party
         */
        ANY
    }
    
    /**
     * Array containing order of parties in chain
     */
    public static PartyType[] partyOrder = 
    {
        PartyType.FARMER,
        PartyType.PROCESSOR,
        PartyType.WAREHOUSE,
        PartyType.VENDOR,
        PartyType.DISTRIBUTOR,
        PartyType.CUSTOMER
    };
    
    /**
     * Creates new party of system
     * @param type Type of party of system
     * @param name Name of party of system
     * @return New party with set name
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *          If there is no available certification authority
     */
    public Party createParty(PartyType type, String name) throws NoCAAvailableException
    {
        Party reti = null;
        switch (type)
        {
            case FARMER:
                reti = this.createFarmer(name);
                break;
            case PROCESSOR:
                reti = this.createProcessor(name);
                break;
            case WAREHOUSE:
                reti = this.createWarehouse(name);
                break;
            case VENDOR:
                reti = this.createVendor(name);
                break;
            case DISTRIBUTOR:
                reti = this.createDistributor(name);
                break;
            case CUSTOMER:
                reti = this.createCustomer(name);
                break;
        }
        return reti;
    }
    
    /**
     * Creates farmer
     * @param name Name of farmer
     * @return New farmer with set name
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
                If there is no available certification authority
     */
    public Farmer createFarmer(String name) throws NoCAAvailableException
    {
        return new Farmer(name);
    }
    
    /**
     * Creates processor
     * @param name Name of processor
     * @return New processor with set name
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
                If there is no available certification authority
     */
    public Processor createProcessor(String name) throws NoCAAvailableException
    {
        return new Processor(name);
    }
    
    /**
     * Creates warehouse of foodstuff
     * @param name Name of warehouse
     * @return New warehouse with set name
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
                If there is no available certification authority
     */
    public Warehouse createWarehouse(String name) throws NoCAAvailableException
    {
        return new Warehouse(name);
    }
    
    /**
     * Creates vendor of foodstuff
     * @param name Name of vendor
     * @return New vendor with set name
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
                If there is no available certification authority
     */
    public Vendor createVendor(String name) throws NoCAAvailableException
    {
        return new Vendor(name);
    }
    
    /**
     * Creates distributor of foodstuff
     * @param name Name of distributor
     * @return New distributor with set name
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
                If there is no available certification authority
     */
    public Distributor createDistributor(String name) throws NoCAAvailableException
    {
        return new Distributor(name);
    }
    
    /**
     * Creates customer of foodstuff
     * @param name Name of customer
     * @return New customer with set name
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
                If there is no available certification authority
     */
    public Customer createCustomer(String name) throws NoCAAvailableException
    {
        return new Customer(name);
    }
    
    /**
     * Resolves type of entered party
     * @param party Party which type will be resolved
     * @return Type of party
     * @throws NotSuchAPartyTypeExistsException Throws when wrong party has been entered
     */
    public static PartyType getType(Party party) throws NotSuchAPartyTypeExistsException
    {
        PartyType reti = null;
        switch (party.getClass().getName())
        {
            case "cz.cvut.fel.skodaj.b3b36omo.foodchain.parties.Farmer":
                reti = PartyType.FARMER;
                break;
            case "cz.cvut.fel.skodaj.b3b36omo.foodchain.parties.Processor":
                reti = PartyType.PROCESSOR;
                break;
            case "cz.cvut.fel.skodaj.b3b36omo.foodchain.parties.Warehouse":
                reti = PartyType.WAREHOUSE;
                break;
            case "cz.cvut.fel.skodaj.b3b36omo.foodchain.parties.Vendor":
                reti = PartyType.VENDOR;
                break;
            case "cz.cvut.fel.skodaj.b3b36omo.foodchain.parties.Distributor":
                reti = PartyType.DISTRIBUTOR;
                break;
            case "cz.cvut.fel.skodaj.b3b36omo.foodchain.parties.Customer":
                reti = PartyType.CUSTOMER;
                break;
        }
        if (reti == null)
        {
            throw new NotSuchAPartyTypeExistsException(party);
        }
        return reti;
    }
    
    /**
     * Translates party type from string to {@code PartyType}
     * @param type Type which will be returned
     * @return PartyType as value of enumeration
     */
    public static PartyType translateType(String type)
    {
        PartyType reti = null;
        switch(type.toUpperCase())
        {
            case "FARMER":
                reti = PartyType.FARMER;
                break;
            case "PROCESSOR":
                reti = PartyType.PROCESSOR;
                break;
            case "WAREHOUSE":
                reti = PartyType.WAREHOUSE;
                break;
            case "VENDOR":
                reti = PartyType.VENDOR;
                break;
            case "DISTRIBUTOR":
                reti = PartyType.DISTRIBUTOR;
                break;
            case "CUSTOMER":
                reti = PartyType.CUSTOMER;
                break;
        }
        
        return reti;
    }
    
    /**
     * Translates {@link cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory.PartyType} to string
     * @param p Party which type will be translated
     * @return String containing translation of party type
     */
    public static String translateToString(Party p)
    {
        String reti  = "";
        switch(p.getType())
        {
            case FARMER:
                reti = "FARMER";
                break;
            case PROCESSOR:
                reti = "PROCESSOR";
                break;
            case WAREHOUSE:
                reti = "WAREHOUSE";
                break;
            case VENDOR:
                reti = "VENDOR";
                break;
            case DISTRIBUTOR:
                reti = "DISTRIBUTOR";
                break;
            case CUSTOMER:
                reti = "CUSTOMER";
                break;
            case ANY:
                reti = "ANY";
                break;
            default:
                reti = "<UNKNOWN>";
                break;
        }
        return reti;
    }
    
}
