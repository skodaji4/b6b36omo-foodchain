/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing channel for making payments
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class PaymentChannel extends Channel
{
    /**
     * List of all made payments
     */
    private List<Payment> payments;
    
    /**
     * Creates new channel for making payments
     */
    public PaymentChannel()
    {
        super("MONEY", PartyFactory.PartyType.ANY);
        this.payments = new ArrayList<>();
    }
    
    /**
     * Makes payment between parties
     * @param pay Pay which will be done between parties
     * @return {@code TRUE} if payment has finished successfully,
     *         {@code FALSE} otherwise
     */
    public boolean makePayment(Payment pay)
    {
        boolean reti = false;
        int parties_served = 0;
        for (Party p : this.parties)
        {
            if (p.getId() == pay.getReceiver().getId())
            {
                p.income(pay.getAmount());
                parties_served++;
                p.addPaymentReport(pay);
            }
            if (p.getId() == pay.getSender().getId())
            {
                p.charge(pay.getAmount());
                parties_served++;
                if (p.getMoney() < 0)
                {
                    World.getInstance().addSecurityReport(new Report("Party " + p.getName() + " is in debt (" + p.getMoney() + ")"));
                }
                p.addPaymentReport(pay);
            }
            if (parties_served == 2)
            {
                reti = true;
                break;
            }
            
        }
        this.payments.add(pay);
        return reti;
    }
    
    /**
     * Gets list of all made payments
     * @return List of all made payments
     */
    public List<Payment> getPayments()
    {
        return this.payments;
    }
}
