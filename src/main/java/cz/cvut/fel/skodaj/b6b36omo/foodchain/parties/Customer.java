/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Foodstuff;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;

/**
 * Class representing customer of foodstuff in food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Customer extends AbstractParty
{
    /**
     * Creates new customer of foodstuff
     * @param name Name of new customer
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *         If there is no available certification authority
     */
    public Customer(String name) throws NoCAAvailableException
    {
        super(name, PartyFactory.PartyType.CUSTOMER);
    }
    
    /**
     * Receives foodstuff from party
     * @param f Foodstuff which will be received
     * @param p Party from which will be foodstuff received
     */
    public void receiveFoodStuff(Foodstuff f, Party p)
    {
        f.finish();
        this.reports.add(new Report("Received " +  f.getName() + " from " + p.getName()));
    }
}
