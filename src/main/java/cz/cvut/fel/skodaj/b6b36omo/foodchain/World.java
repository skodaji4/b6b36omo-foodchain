/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.config.Configuration;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Channel;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Foodstuff;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Request;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Customer;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Payment;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.PaymentChannel;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Transaction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.ScammerVendor;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.security.CertificationAuthority;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class representing "context" for whole project
 * It is singleton so it can be instanced only once in whole project
 * (i am not discussing possibility of existence of "more worlds" in real life, for
 * this project there is only one world)
 * This is also facade of system for any calls from outside
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class World {
    /**
     * Storing one instance of world
     */
    private static World instance = null;
    
    /**
     * Configuration of whole system
     */
    private Configuration config = null;
    
    /**
     * List of all used integers
     */
    private List<Integer> usedIds;
    
    /**
     * Instance of random class
     */
    private Random rnd;
    
    /**
     * List of all trusted certification authorities in world
     */
    private List<CertificationAuthority> trustedAuthorities;
    
    
    /**
     * Channels available
     */
    private Map<String, Channel> channels;
    
    /**
     * List of all available parties
     */
    private List<Party> parties;
    
    /**
     * Factory used for making parties
     */
    private PartyFactory pF;
    
    /**
     * Offset to actual time
     */
    private Duration timeOffset;
    
    /**
     * List of all foodstuff in system
     */
    private List<Foodstuff> foodstuff;
    
    /**
     * List of all reports connected with security
     */
    private List<Report> securityReports;
    
    /**
     * Channel for making payments between parties
     */
    private PaymentChannel paymentChannel;
    
    /**
     * Number of made transaction reports
     */
    private int transactionReports = 0;
    
    /**
     * Private constructor of class (as it is singleton)
     */
    private World()
    {
        this.config = Configuration.getConfig();
        this.usedIds = new ArrayList<>();
        this.rnd = new Random();
        this.trustedAuthorities = new ArrayList<>();
        this.channels = new HashMap<>();
        this.parties = new ArrayList<>();
        this.pF = new PartyFactory();
        this.foodstuff = new ArrayList<>();
        this.timeOffset = Duration.ofNanos(0);
        this.securityReports = new ArrayList<>();
        this.paymentChannel = new PaymentChannel();
    }
    
    /**
     * Gets instance of world
     * @return Instance world
     */
    public static World getInstance()
    {
        if (World.instance == null)
        {
            World.instance = new World();
        }
        return World.instance;
    }
    
    /**
     * Gets configuration of system
     * @return Configuration of system
     */
    public Configuration getConfig()
    {
        return this.config;
    }
    
    /**
     * Gets unique integer in whole world
     * @return Unique integer in whole world
     */
    public int getIdentifier()
    {
        int reti = Math.abs(this.rnd.nextInt());
        while (this.usedIds.contains(reti))
        {
            reti = this.rnd.nextInt();
        }
        this.usedIds.add(reti);
        return reti;
    }
    
    /**
     * Creates new trusted certification authority
     * @param name Name of new certification authority
     * @return New certification authority
     */
    public CertificationAuthority createCertificationAuthority(String name)
    {
        CertificationAuthority reti = new CertificationAuthority(name);
        this.trustedAuthorities.add(reti);
        return reti;
    }
    
    /**
     * Loads world settings from configuration
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *         If there is no available certification authority throws exception
     */
    public void setUpFromConfig() throws NoCAAvailableException
    {
        this.loadCAs();
        if (this.config.getScammerVendor())
        {
            this.parties.add(new ScammerVendor("TotallyLegitNoScammer Ltd."));
        }
        this.loadParties();
        this.loadFoodstuff();

    }
    
    /**
     * Loads certification authorities 
     */
    private void loadCAs()
    {
        for (String ca : this.config.getCAs())
        {
            this.createCertificationAuthority(ca);
        }
    } 
    
    /**
     * Loads parties
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *         If there is no available certification authority throws exception
     */
    private void loadParties() throws NoCAAvailableException
    {
        cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory pf = new cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory();
        for (String[] party : this.config.getParties())
        {
            this.createParty(party[0], party[1]);
        }
    }
    
    /**
     * Loads foodstuff from configuration
     */
    private void loadFoodstuff()
    {
        for (Foodstuff f : this.config.getFoodstuffs())
        {
            this.foodstuff.add(f);
        }
    }
    
    /**
     * Gets any available certification authority
     * @return Any available certification authority
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *         If there is no available certification authority throws exception
     */
    public CertificationAuthority getCA() throws NoCAAvailableException
    {
        int min = Integer.MAX_VALUE;
        CertificationAuthority reti = null;
        for (CertificationAuthority ca: this.trustedAuthorities)    
        {
            if (ca.getCertificatesCount() < min)                    // Load balancing
            {
                min = ca.getCertificatesCount();
                reti = ca;
                //if (min == 0)
                //{
                //    break;
                //}
            }
        }        
        if (reti == null)
        {
            throw new NoCAAvailableException();
        }
        return reti;
    }
    
    /**
     * Gets actual time in world
     * @return Actual time in world
     */
    public LocalDateTime getTime()
    {
        LocalDateTime reti = LocalDateTime.now();
        reti = reti.plus(this.timeOffset);
        return reti;
    }
    
    /**
     * Moves time forward
     * @param time How much it will be forwarded
     */
    public void moveTime(Duration time)
    {
        this.timeOffset = this.timeOffset.plus(time);
    }
    
    /**
     * Creates new party of system
     * @param type Type of party
     * @param name Name of party
     * @return New created party
     * @throws NoCAAvailableException 
     *          If there is no available certification authority
     */
    public Party createParty(String type, String name) throws NoCAAvailableException
    {
        Party reti = this.pF.createParty(PartyFactory.translateType(type), name);
        this.parties.add(reti);
        return reti;
    }
    
    /**
     * Gets list of customers in system
     * @return List of customers in system
     */
    public List<Customer> getCustomers()
    {
        List<Customer> reti = new ArrayList<>();
        for (Party p: this.parties)
        {
            if (p.getType() == PartyFactory.PartyType.CUSTOMER)
            {
                reti.add((Customer)p);
            }
        }
        return reti;
    }
    
    /**
     * Creates new channel or gets already existing one for communication between parties
     * @param goal Goal of channel
     * @param type Type of parties which can join channel
     * @return Newly created or already existing channel
     */
    private Channel getChannel(String goal, PartyFactory.PartyType type)
    {
        Channel reti = null;
        if (this.channels.containsKey(goal) == false)
        {
            reti = new Channel(goal, type);
            this.channels.put(goal, reti);
            this.notifyParties(reti);
        }
        else
        {
            reti = this.channels.get(goal);
        }
        return reti;
    }
    
    /**
     * Offers to all parties joining channel
     * @param ch Channel which can be joined
     */
    private void notifyParties(Channel ch)
    {
        while (ch.getCount() <= 0)
        {
            for (Party p : this.parties)
            {
                p.joinChannel(ch);
            }
        }
    }
    
    /**
     * Gets actual time as long integer value
     * @return Actual time as long integer value
     */
    public long getLongTime()
    {
        return World.getDateAsLong(this.getTime());
    }
    
    /**
     * Changes date time to long integer
     * @param date Date which will be converted into long integer
     * @return Date as long integer
     */
    public static long getDateAsLong(LocalDateTime date)
    {
        long reti = 0;
        reti += date.getYear()*(10^10);
        reti += date.getMonthValue()*(10^8);
        reti += date.getDayOfMonth()*(10^6);
        reti += date.getHour()*(10^4);
        reti += date.getMinute()*(10^2);
        reti += date.getSecond();
        return reti;
    }
    
    /**
     * Makes foodstuff by world
     * @param c Customer which is requesting foodstuff
     * @param f Foodstuff which should be done
     */
    public void makeFoodstuff(Customer c, Foodstuff f)
    {
        this.initPaymentChannel();
        f.setCustomer(c);
        if (f.hasNext())
        {
            Instruction instruction = f.next();
            PartyFactory.PartyType type = instruction.getType();
            Request request = new Request(c, instruction, type, f);
            this.handleRequest(request);
        }
    }
    
    /**
     * Handles request by world
     * @param req Request which should be done
     */
    public void handleRequest(Request req)
    {
        boolean found_channel = false;
        int i = 0;
        Channel channel = this.getChannel(req.getGoal().getOutput(), req.getType());
        while (found_channel == false && i < this.channels.size())
        {
            channel = this.getChannel(req.getGoal().getOutput(), req.getType());
            if (channel.canHandleRequest(req))
            {
                found_channel = true;
                //break;
            }
            i++;
        }
        if (found_channel)
        {
            if (this.config.getTransactionReport())
            {
                this.makeTransactionReport();
            }
            
            channel.handleRequest(req);
        }
    }
    
    /**
     * Makes report about all parties and transactions
     */
    public void makeTransactionReport()
    {
        this.transactionReports++;
        String filename = this.config.getDefaultPath() + "T_REPORT_" + this.transactionReports + ".TXT";
        String write = "TRANSACTION REPORT \n===========================================================\n";
        write += "Number: " + this.transactionReports + "\n";
        write += "--- PARTIES -----------------------------------------------\n";
        write += String.format("%1$40s", "[PARTY]") + "\t" + String.format("%1$16s", "[TYPE]") + "\t[MONEY]\n";
        for (Party p : this.parties)
        {
            write += String.format("%1$40s", p.getName()) + "\t" +  String.format("%1$16s",PartyFactory.translateToString(p)) + "\t" + p.getMoney() + "\n";
        }
        write += "--- TRANSACTIONS ------------------------------------------\n";
        write += String.format("%1$26s", "[TIME]") + "\t " + String.format("%1$40s", "[PARTY]") + "\t[OPERATION]\n";
        for (Foodstuff f : this.foodstuff)
        {
            for (Transaction t : f.getTransactions())
            {
                write += String.format("%1$26s",t.getTime().toString()) + "\t" + String.format("%1$40s",t.getParty().getName()) + "\t" + t.getInstruction().getName() + "\n";
            }
        }
        write += "--- PAYMENTS ----------------------------------------------\n";
        write +=  String.format("%1$40s", "[SENDER]") + "\t " + String.format("%1$40s", "[RECEIVER]") + "\t[AMOUNT]\n ";
        for (Payment p : this.paymentChannel.getPayments())
        {
            write += String.format("%1$40s", p.getSender().getName()) + "\t" + String.format("%1$40s", p.getReceiver().getName()) + "\t" + p.getAmount() + "\n";
        }
        write += "--- (end of report) ---------------------------------------\n";
         try {
            Files.write(Paths.get(filename), write.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    
    
    
    /**
     * Gets list of all foodstuff in system
     * @return List of all foodstuff in system
     */
    public List<Foodstuff> getFoodstuff()
    {
        return this.foodstuff;
    }
    
    /**
     * Makes foodstuff report and writes it into file
     * @param food Foodstuff which report will be generated
     * @param filename Name of output file
     */
    public void makeFoodstuffReport(Foodstuff food, String filename)
    {
        try {
            Files.write(Paths.get(filename), food.makeReport().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Makes party report and writes it into file
     * @param party Party which report will be generated
     * @param filename Name of output file
     */
    public void makePartyReport(Party party, String filename)
    {
        try {
            Files.write(Paths.get(filename), party.makeReport().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Gets list of all parties in system
     * @return List of all parties in system
     */
    public List<Party> getParties()
    {
        return this.parties;
    }
    
    /**
     * Makes payment between parties
     * @param pay Payment which will be made between parties
     */
    public void makePayment(Payment pay)
    {
        this.paymentChannel.makePayment(pay);
    }
    
    /**
     * Adds security report
     * @param report Security report
     */
    public void addSecurityReport(Report report)
    {
        this.securityReports.add(report);
    }
    
    /**
     * Generates security report
     * @param filename Name of output file
     */
    public void generateSecurityReport(String filename)
    {
        String write = "SECURITY REPORT \n===========================================================\n";
        for (Report rep : this.securityReports)
        {
            write += rep.toString() + "\n";
        }
        write += "--- (end of report) ---------------------------------------\n";
                try {
            Files.write(Paths.get(filename), write.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Initializes payment channel
     */
    private void initPaymentChannel()
    {
        for (Party p: this.parties)
        {
            p.joinChannel(this.paymentChannel);
        }
    }
    
}
