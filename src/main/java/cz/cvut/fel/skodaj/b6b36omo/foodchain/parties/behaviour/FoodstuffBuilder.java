/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import java.time.Duration;

/**
 * Class representing builder of instruction list which leads to foodstuff
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class FoodstuffBuilder
{
    /**
     * List with instructions
     */
    private InstructionSet instructionSet;
    
    /**
     * Creates new builder of instruction list
     */
    public FoodstuffBuilder()
    {
        this.instructionSet = new InstructionSet();
    }
    
    /**
     * Adds new growing process
     * @param plant Plant which will be grown
     * @param time Time needed for growing
     * @param output Output of growing process
     * @param cost Cost of growing
     * @return List of instructions with added instruction
     */
    public FoodstuffBuilder grow(String plant, Duration time, String output, int cost)
    {
        Grow g = new Grow(plant, time, output, cost);
        this.instructionSet.add(g);
        return this;
    }
    
    /**
     * Adds new action which is done at processor
     * @param output Output of process
     * @param time Time needed for process
     * @param cost Cost of processing
     * @return List of instructions with added instruction
     */
    public FoodstuffBuilder process(String output, Duration time, int cost)
    {
        this.instructionSet.add(new Process(output, time, cost));
        return this;
    }
    
    /**
     * Adds new storing
     * @param output Output of storing
     * @param time Time of storing
     * @param temperature Temperature of storing
     * @param humidity Humidity of storing
     * @param cost Cost of storing
     * @return List of instructions with added instruction
     */
    public FoodstuffBuilder store(String output, Duration time, double temperature, double humidity, int cost)
    {
        this.instructionSet.add(new Store(time, temperature, humidity, cost, output));
        return this;
    }
    
    /**
     * Adds new selling instruction
     * @param output Output of selling
     * @param time Time needed for selling
     * @param price Price of foodstuff
     * @param cost Cost of selling
     * @return List of instructions with added instruction
     */
    public FoodstuffBuilder sell(String output, Duration time, int price, int cost)
    {
        this.instructionSet.add(new Sell(output, time, price, cost));
        return this;
    }
    
    /**
     * Adds new distribution
     * @param distance Distance of distribution
     * @param speed Speed of distribution
     * @param cost Cost of distribution
     * @param output Output of instruction
     * @return List of instructions with added instruction
     */    
    public FoodstuffBuilder distribute(String output, int distance, int speed, int cost)
    {
        this.instructionSet.add(new Distribute(distance, speed, cost, output));
        return this;
    }
    
    /**
     * Gets list of instructions
     * @return List of instructions
     */
    public InstructionSet getInstructions()
    {
        return this.instructionSet;
    }
    
}
