/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.security.Hash;
import java.time.LocalDateTime;

/**
 * Class representing transaction made in food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Transaction
{
    /**
     * Party which performed transaction
     */
    private Party party;
    
    /**
     * Which instruction has been performed during transaction
     */
    private Instruction instruction;
    
    /**
     * Previous transaction
     */
    private Transaction prevoiusTransaction;
    
    /**
     * Time when has been transaction made
     */
    private LocalDateTime time;
    
    /**
     * Hash of transaction
     */
    private Hash hash;
    
    /**
     * Creates new transaction
     * @param p Party which done instruction
     * @param i Instruction which has been made
     * @param t Previous transaction
     */
    public Transaction(Party p, Instruction i, Transaction t)
    {
        this.party = p;
        this.instruction = i;
        this.prevoiusTransaction = t;
        this.time = World.getInstance().getTime();
        this.hash = new Hash(this);
    }
    
    /**
     * Gets party which made transaction
     * @return Party which made transaction
     */
    public Party getParty()
    {
        return this.party;
    }
    
    /**
     * Gets time of transaction
     * @return Time of transaction
     */
    public LocalDateTime getTime()
    {
        return this.time;
    }
    
    /**
     * Gets previous transaction
     * @return Previous transaction
     */
    public Transaction getPrevoius()
    {
        return this.prevoiusTransaction;
    }
    
   
    /**
     * Gets performed instruction
     * @return Performed instruction
     */
    public Instruction getInstruction()
    {
        return this.instruction;
    }
}
