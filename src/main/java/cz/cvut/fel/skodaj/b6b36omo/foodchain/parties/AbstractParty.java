/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Channel;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Payment;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Request;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Transaction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.security.Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class abstracting all parties in system and implementing same base of all parties
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public abstract class AbstractParty implements Party
{
    /**
     * Name of party
     */
    protected String name;
    
    /**
     * Unique identifier of party
     */
    protected int id;
    
    /**
     * Actual money amount of party
     */
    protected int money;
    
    /**
     * Type of party
     */
    protected PartyFactory.PartyType type;
    
    /**
     * Last request done by party
     */
    protected Request req;
    
    /**
     * All requests done by party
     */
    protected List<Request> requests;
    
    /**
     * List of reports connected with party
     */
    protected List<Report> reports;
    
    /**
     * Certificate of party
     */
    private Certificate cert;
    
    /**
     * Creates new party of system
     * @param name Name of party
     * @param type Type of party
     * @throws cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException
     *          Thrown when there is no available CA for issuing certificates
     */
    public AbstractParty(String name, PartyFactory.PartyType type) throws NoCAAvailableException
    {
        this.name = name;
        this.id = World.getInstance().getIdentifier();
        this.cert = World.getInstance().getCA().issueCertificate(this);
        this.type = type;
        this.requests = new ArrayList<>();
        this.reports = new ArrayList<>();
    }
    
    /**
     * Gets name of party
     * @return Name of party
     */
    @Override
    public String getName()
    {
        return this.name;
    }
    
    /**
     * Gets identifier of party
     * @return Identifier of party
     */
    @Override
    public int getId()
    {
        return this.id;
    }
    
    /**
     * Decision of joining channel
     * @param ch Channel which can be joined
     */
    @Override
    public void joinChannel(Channel ch)
    {
        if ("MONEY".equals(ch.getResult()))
        {
            ch.join(this);
        }
        else
        {
            Random rnd = new Random();
            int random = rnd.nextInt(100);
            if (random < World.getInstance().getConfig().getProb() && (ch.getType() == this.type || ch.getType() == PartyFactory.PartyType.ANY))
            {
                ch.join(this);
            }
        }
    }
    
        
    /**
     * Gets type of party
     * @return Type of party
     */
    @Override
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
    
    /**
     * Gets one entry to hash
     * @return Entry to hash
     */
    @Override
    public int getSubHash()
    {
        int reti = 0;
        reti += this.cert.getId();
        reti *= World.getDateAsLong(this.cert.getTime());
        return reti;
    }
    
    /**
     * Gets how much money party has
     * @return How much money party has
     */
    @Override
    public int getMoney()
    {
        return this.money;
    }
    
    /**
     * Charges party by price
     * @param value Price of which will be party charged
     */
    @Override
    public void charge(int value)
    {
        this.money -= value;
    }
    
    /**
     * Handles request
     * @param req Request which should be handled
     */
    @Override
    public void handleRequest(Request req)
    {
        if (req.getFoodstuff().getLastTransaction() != null)
        {
            Payment pay = new Payment(this, req.getFoodstuff().getLastTransaction().getParty(), req.getGoal().getCost());
            World.getInstance().makePayment(pay);
        }
        this.performInstruction(this.req.getGoal());
    }
    
    
    /**
     * Decision if party handles request
     * @param req Request which should be handled
     * @return {@code TRUE} if party wants handle request,
     *         {@code FALSE} otherwise
     */
    @Override
    public boolean canHandleRequest(Request req)
    {
        boolean reti = false;
        Random rnd = new Random();
        if (
                (req.getDestination() != null &&
                req.getDestination().getId() == this.id) ||
                (req.getDestination() == null &&
                rnd.nextInt(100) < World.getInstance().getConfig().getProb() &&
                (req.getType() == this.getType() || req.getType() == PartyFactory.PartyType.ANY))
           )
        {
            reti = true;
            this.req = req;
            this.requests.add(req);
        }
        return reti;
    }
    
    /**
     * Performs instruction by party
     * @param instruction Which instruction will be performed by party
     */
    protected void performInstruction(Instruction instruction)
    {
        World.getInstance().moveTime(instruction.getTime());
    }
    
    /**
     * Sends request to do next instruction
     */
    protected void doNext()
    {
        Transaction t = new Transaction(this, this.req.getGoal(), this.req.getFoodstuff().getLastTransaction());
        this.req.getFoodstuff().addTransaction(t);
        if (this.req.getFoodstuff().hasNext())
        {            
            Instruction next = this.req.getFoodstuff().next();
            Request req = new Request(this, next, next.getType(), this.req.getFoodstuff());
            World.getInstance().handleRequest(req);
        }
    }
    
    /**
     * Makes report of party
     * @return Report of party
     */
    @Override
    public String makeReport()
    {
        String reti = "PARTY REPORT"  + "\n===========================================================\n";
        reti += "Name:  " + "\t" + this.getName() + "\n";
        reti += "Type:  " + "\t";
        switch(this.type)
        {
            case FARMER:
                reti += "FARMER";
                break;
            case PROCESSOR:
                reti += "PROCESSOR";
                break;
            case WAREHOUSE:
                reti += "WAREHOUSE";
                break;
            case VENDOR:
                reti += "VENDOR";
                break;
            case DISTRIBUTOR:
                reti += "DISTRIBUTOR";
                break;
            case CUSTOMER:
                reti += "CUSTOMER";
                break;
            default:
                reti += "<UNKNOWN>";
                break;
        }
        reti += "\n";
        reti += "Money: \t" + this.money + "\n";
        reti += "-----------------------------------------------------------\n";
        for (Report r : this.reports)
        {
            reti += r.toString() + "\n";
        }
        reti += "--- (end of report) ---------------------------------------\n";
        return reti;
    }
    
    /**
     * Adds money to party
     * @param value Amount of money which will be added to party
     */
    @Override
    public void income(int value)
    {
        this.money += value;
    }
    
   /**
     * Adds payment report to party
     * @param pay Payment which should be reported
     */
    @Override
    public void addPaymentReport(Payment pay)
    {
        if (pay.getSender().getId() == this.getId())
        {
            this.addChargeReport(pay);
        }
        else if (pay.getReceiver().getId() == this.getId())
        {
            this.addIncomeReport(pay);
        }
    }
    
    /**
     * Adds income report
     * @param pay Payment realizing income
     */
    protected void addIncomeReport(Payment pay)
    {
        this.reports.add(new Report("Received " + pay.getAmount() + " money from " + pay.getSender().getName()));
    }
    
    /**
     * Adds charge report
     * @param pay Payment realizing charge
     */
    protected void addChargeReport(Payment pay)
    {
        this.reports.add(new Report("Sent " + pay.getAmount() + " money to " + pay.getReceiver().getName()));
    }
    
    
}
