/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.config;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Foodstuff;

/**
 * Class storing all necessary configuration for food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Configuration
{
    
    /**
     * Instance of configuration (only one in whole system)
     */
    private static Configuration instance = null;
    
    /**
     * List of available certification authorities
     */
    private String[] CAs;
    
    /**
     * List of available parties and its types
     */
    private String[][] parties;
    
    /**
     * Default path for reporting
     */
    private String defaultPath;
    
    /**
     * Probability of acceptance (in percents)
     */
    private int acceptanceProbability = 50;
    
    /**
     * List of foodstuff which will be made
     */
    private Foodstuff[] foodstuffs;
    
    /**
     * Defines, whether transaction reports should be made
     */
    private boolean makeTransactionReport;
    
    /**
     * Defines, whether there should be scammer vendor
     */
    private boolean scammerVendor;
    
    
    private Configuration()
    {
        //pass (singleton)
    };
    
    /**
     * Gets configuration of whole system
     * @return Configuration of whole system
     */
    public static Configuration getConfig()
    {
        if (Configuration.instance == null)
        {
            Configuration.instance = new Configuration();
        }
        return Configuration.instance;
    }
    
    /**
     * Loads configuration from object
     * @param config Object which stores configuration
     */
    public void loadConfig(TemplateConfiguration config)
    {
        this.CAs = config.CAs;
        this.parties = config.parties;
        this.foodstuffs = config.foodstuffs;
        this.defaultPath = config.defaultPath;
        this.makeTransactionReport = config.makeTransactionReport;
        this.scammerVendor = config.scammerVendor;
    }
    
    /**
     * Gets names of certification authorities
     * @return Array with names of certification authorities
     */
    public String[] getCAs()
    {
        return this.CAs;
    }
    
    /**
     * Gets names of parties with types
     * @return Array with names and types of parties
     */
    public String[][] getParties()
    {
        return this.parties;
    }
    
    /**
     * Gets acceptance probability
     * @return Acceptance probability
     */
    public int getProb()
    {
        return this.acceptanceProbability;
    }
    
    /**
     * Gets list of foodstuffs which will be made
     * @return List of foodstuffs which will be made
     */
    public Foodstuff[] getFoodstuffs()
    {
        return this.foodstuffs;
    }
    
    /**
     * Gets default path for reporting
     * @return Default path for reporting
     */
    public String getDefaultPath()
    {
        return this.defaultPath;
    }
    
    /**
     * Gets whether transaction reports should be made or not
     * @return {@code TRUE} if transaction reports should be made,
     *         {@code FALSE} otherwise
     */
    public boolean getTransactionReport()
    {
        return this.makeTransactionReport;
    }
    
    /**
     * Gets, whether there should be scammer vendor in system
     * @return {@code TRUE} if there should be scammer vendor in system,
     *         {@code FALSE} otherwise
     */
    public boolean getScammerVendor()
    {
        return this.scammerVendor;
    }
}
