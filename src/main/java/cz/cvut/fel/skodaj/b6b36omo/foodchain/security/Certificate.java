/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.security;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import java.time.LocalDateTime;

/**
 * Class representing certificate used for 
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Certificate
{
    /**
     * Unique identifier of certificate
     */
    private int id;
    
    /**
     * Certification authority which issued this certificate
     */
    private CertificationAuthority authority;
    
    /**
     * Owner of certificate
     */
    private Party owner;
    
    /**
     * Date time of creation of certificate
     */
    private LocalDateTime created;
    
    /**
     * Creates new certificate
     * @param CA Authority which can issue the certificate
     * @param p Party which asks for certificate
     */
    public Certificate(CertificationAuthority CA, Party p)
    {
        this.id = World.getInstance().getIdentifier();
        this.owner = p;
        this.authority = CA;
        this.created = World.getInstance().getTime();
    }
    
    /**
     * Gets identifier of certificate
     * @return Identifier of certificate
     */
    public int getId()
    {
        return this.id;
    }
    
    /**
     * Gets owner of certificate
     * @return Owner of certificate
     */
    public Party getParty()
    {
        return this.owner;
    }
    
    /**
     * Gets Certification Authority which issued certificate
     * @return Authority which issued certificate
     */
    public CertificationAuthority getAuthority()
    {
        return this.authority;
    }
    
    /**
     * Gets time of creation of certificate
     * @return Time of creation of certificate
     */
    public LocalDateTime getTime()
    {
        return this.created;
    }
}