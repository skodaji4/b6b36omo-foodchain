/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NotSuchAPartyTypeExistsException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Customer;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Distribute;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Grow;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.InstructionSet;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Sell;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Store;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class representing foodstuff in food chain
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Foodstuff
{
    /**
     * Name of foodstuff
     */
    private String name;
    
    /**
     * Flag if foodstuff is done
     */
    private boolean done = false;
    
    /**
     * Instructions which leads to foodstuff
     */
    private InstructionSet instructions;
    
    /**
     * List of transactions made with foodstuff
     */
    private List<Transaction> transactions;
    
    /**
     * List of all reports connected with foodstuff
     */
    private List<Report> report;
    
    /**
     * Customer which requires foodstuff
     */
    private Customer customer;
    
    /**
     * Creates new foodstuff
     * @param name Name of foodstuff
     * @param instructions Instructions how to make foodstuff
     */
    public Foodstuff(String name, InstructionSet instructions)
    {
        this.name = name;
        this.instructions = instructions;
        this.transactions = new ArrayList<>();
        this.report = new ArrayList<>();
    }
    
    /**
     * Checks, whether there is some remaining instruction
     * @return {@code TRUE} if there is some unfinished instruction,
     *         {@code FALSE} otherwise
     */
    public boolean hasNext()
    {
        boolean reti = this.instructions.hasNext();
        if (reti == false)
        {
            this.done = true;
        }
        return reti;
    }
    
    /**
     * Get next instruction which should be done
     * @return Instruction which should be done
     */
    public Instruction next()
    {
        return this.instructions.next();
    }
    
    /**
     * Checks whether foodstuff is done
     * @return {@code TRUE} if foodstuff is done,
     *         {@code FALSE} otherwise
     */
    public boolean checkIfDone()
    {
        return this.done;
    }
    
    /**
     * Gets name of foodstuff
     * @return name of foodstuff 
     */
    public String getName()
    {
        return this.name;
    }
    
    /**
     * Adds transaction which has been made with foodstuff
     * @param t Transaction which has been made with foodstuff
     */
    public void addTransaction(Transaction t)
    {
        try {
            this.transactions.add(t);
            this.addReport(t);
        } catch (NotSuchAPartyTypeExistsException ex) {
            Logger.getLogger(Foodstuff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Adds report to foodstuff
     * @param t Transaction containing party 
     * @throws NotSuchAPartyTypeExistsException If there is no party type as in transaction 
     */
    private void addReport(Transaction t) throws NotSuchAPartyTypeExistsException
    {
        Report r = null;
        PartyFactory.PartyType type = t.getParty().getType();
        switch (type)
        {
            case FARMER:
                r = new Report(this.generateGrowString(t));
                break;
            case PROCESSOR:
                r = new Report(this.generateProcessString(t));
                break;    
            case WAREHOUSE:
                r = new Report(this.generateStoreString(t));
                break;
            case VENDOR:
                r = new Report(this.generateSellString(t));
                break;
            case DISTRIBUTOR:
                r = new Report(this.generateDistributeString(t));
                break;
            default:
                r = new Report("ERROR! Unexpected action!");
                break;
        }
        if (r != null)
        {
            this.report.add(r);
        }
        
    }
    
    /**
     * Generates report message for growing
     * @param t Transaction containing instruction
     * @return Report message
     */
    private String generateGrowString(Transaction t)
    {
         Grow g = (Grow)t.getInstruction();
         return "Growing " + g.getOutput() + " from " + g.getPlant() + " at " + t.getParty().getName() + " for " + t.getInstruction().getTime().toString();
    }
    
    /**
     * Generates report message for distributing
     * @param t Transaction containing instruction
     * @return Report message
     */    
    private String generateDistributeString(Transaction t)
    {
        Distribute d = (Distribute)t.getInstruction();
        return "Distributing " + d.getOutput() + " from " + t.getPrevoius().getParty().getName() + " to " + this.customer.getName() + " using " + t.getParty().getName() + " (distance: " + d.getDistance() + "; time: " + d.getTime() + ")";
    }
    
    /**
     * Generates report message for processing
     * @param t Transaction containing instruction
     * @return Report message
     */    
    private String generateProcessString(Transaction t)
    {
        return "Processing " + t.getPrevoius().getInstruction().getOutput() + " to " + t.getInstruction().getOutput() + " at " + t.getParty().getName() + " for " + t.getInstruction().getTime();         
    }
    
    /**
     * Generates report message for selling
     * @param t Transaction containing instruction
     * @return Report message
     */    
    private String generateSellString(Transaction t)
    {
        Sell s = (Sell)t.getInstruction();
        return "Sold " + t.getInstruction().getOutput() + " at " + t.getParty().getName() + " for " + s.getPrice();
    }
    
    /**
     * Generates report message for storing
     * @param t Transaction containing instruction
     * @return Report message
     */    
    private String generateStoreString(Transaction t)
    {
        Store s = (Store)t.getInstruction();
        return ("Storing " + t.getPrevoius().getInstruction().getOutput() + " at " + t.getParty().getName() + "(humidity: " + s.getHumidity() + "; temperature: " + s.getTemperature() + ") for " + s.getTime());
    }
    
    /**
     * Gets last transaction
     * @return Last transaction
     */
    public Transaction getLastTransaction()
    {
        Transaction reti = null;
        if (this.transactions.size() > 0)
        {
            reti = this.transactions.get((this.transactions.size() - 1));
        }
        return reti;
    }
    
    /**
     * Makes foodstuff report
     * @return Foodstuff report
     */
    public String makeReport()
    {
        String reti = "FOODSTUFF REPORT"  + "\n===========================================================\n";
        reti += "Name:    \t " + this.name + "\n";
        reti += "Customer:\t " + this.customer.getName() + "\n";
        reti += "-----------------------------------------------------------\n";
        for (Report r : this.report)
        {
            reti += r.toString() + "\n";
        }
        reti += "--- (end of report) ---------------------------------------\n";
        return reti;
        
    }
    
    /**
     * Sets customer of foodstuff
     * @param customer Customer of foodstuff
     */
    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }
    
    /**
     * Gets customer of foodstuff
     * @return Customer of foodstuff
     */
    public Customer getCustomer()
    {
        return this.customer;
    }
    
    /**
     * Gets list of all made transaction
     * @return List of all made transaction
     */
    public List<Transaction> getTransactions()
    {
        return this.transactions;
    }
    
    /**
     * Finishes foodstuff
     */
    public void finish()
    {
        this.done = true;
    }
}
