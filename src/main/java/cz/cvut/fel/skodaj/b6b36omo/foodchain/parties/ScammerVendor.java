/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.World;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.exceptions.NoCAAvailableException;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Payment;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Request;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff.Transaction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Report;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Sell;
import java.util.Random;

/**
 * Class representing scammer vendor
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class ScammerVendor extends Vendor
{
    /**
     * Creates new scammer vendor
     * @param name Name of scammer vendor
     * @throws NoCAAvailableException If there is no certification authority available
     */
    public ScammerVendor(String name) throws NoCAAvailableException
    {
        super(name);
    }
    
    /**
     * Performs next instruction
     * (and tries sell foodstuff again)
     */
    @Override
    protected void doNext()
    {
        Transaction t = new Transaction(this, this.req.getGoal(), this.req.getFoodstuff().getLastTransaction());
        this.req.getFoodstuff().addTransaction(t);
        if (this.req.getFoodstuff().hasNext())
        {            
            Instruction next = this.req.getFoodstuff().next();
            Request req = new Request(this, next, next.getType(), this.req.getFoodstuff());
            World.getInstance().handleRequest(req);
        }        
    }
    
    /**
     * Performs selling
     * @param instruction Instruction describing selling
     */
    @Override
    protected void performInstruction(Instruction instruction)
    {
        if (instruction.getType() == this.type)
        {
            super.performInstruction(instruction);
            Sell s = (Sell)instruction;
            this.reports.add(new Report(
            "Selling " + s.getOutput() + " for " + s.getPrice()
            ));                    
            
        }
        this.doNext();
    }
    
    /**
     * Handles request always if type of request is for vendor
     * @param req Request which should be handled
     * @return {@code TRUE} if vendor wants to handle request,
     *         {@code FALSE} otherwise
     */
    @Override
    public boolean canHandleRequest(Request req)
    {
        boolean reti = false;
        if ((req.getType() == this.getType() || req.getType() == PartyFactory.PartyType.ANY) &&
                (new Random()).nextInt(100) < World.getInstance().getConfig().getProb())
        {
            reti = true;
            this.req = req;
            this.requests.add(req);
        }
        return reti;
    }
    
   /**
     * Handles request
     * @param req Request which should be handled
     */
    @Override
    public void handleRequest(Request req)
    {
        //Scammy request
        Request another = new Request(this, req.getGoal(), PartyFactory.PartyType.VENDOR, req.getFoodstuff(), this);
        super.handleRequest(req);
        World.getInstance().handleRequest(another);
        
    }
}
