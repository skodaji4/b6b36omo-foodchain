/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.foodstuff;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.Party;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour.Instruction;

/**
 * Class representing request
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Request
{
    /**
     * Instruction which should be done 
     */
    private Instruction goal;
    
    /**
     * Which type of party can handle request
     */
    private PartyFactory.PartyType type;
    
    /**
     * Destination of request
     */
    private Party destination;
    
    /**
     * Foodstuff which is subject of request
     */
    private Foodstuff foodstuff;
    
    /**
     * Sender of request
     */
    private Party sender;
    
    
    /**
     * Creates new request
     * @param sender Sender of request
     * @param goal Goal of request
     * @param type Which type of party can handle request
     * @param foodstuff Foodstuff which is subject of request
     */
    public Request(Party sender, Instruction goal, PartyFactory.PartyType type, Foodstuff foodstuff)
    {
        this.goal = goal;
        this.type = type;
        this.destination = null;
        this.foodstuff = foodstuff;
        this.sender = sender;
    }
    
    /**
     * Creates new request
     * @param sender Sender of request
     * @param goal Goal of request
     * @param type Which type of party can handle request
     * @param foodstuff Foodstuff which is subject of request
     * @param destination Defines which party must handle request
     */
    public Request(Party sender, Instruction goal, PartyFactory.PartyType type, Foodstuff foodstuff, Party destination)
    {
        this.goal = goal;
        this.type = type;
        this.destination = destination;
        this.foodstuff = foodstuff;
        this.sender = sender;
    }
    
    /**
     * Gets type of party which can handle request
     * @return Type of party which can handle request
     */
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
    
    /**
     * Gets goal of request
     * @return Goal of request
     */
    public Instruction getGoal()
    {
        return this.goal;
    }
    
    /**
     * Gets which party must handle request
     * @return Party which must handle request
     */
    public Party getDestination()
    {
        return this.destination;
    }
    
    /**
     * Gets foodstuff of request
     * @return Foodstuff of request
     */
    public Foodstuff getFoodstuff()
    {
        return this.foodstuff;
    }
    
    /**
     * Gets sender of request
     * @return Sender of request
     */
    public Party getSender()
    {
        return this.sender;
    }
}
