/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import java.time.Duration;

/**
 * Class representing one instruction leading to foodstuff
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public abstract class Instruction {
    
    /**
     * Sets which party can handle instruction
     */    
    protected PartyFactory.PartyType type;
    
    /**
     * Cost of instruction
     */
    protected int cost;
    
    /**
     * Time needed to perform instruction
     */
    protected Duration time;
    
    /**
     * Output of instruction
     */
    protected String output;
    
    /**
     * Creates new instruction
     * @param cost Cost of instruction
     * @param time Time needed for instruction
     * @param output Output of instruction
     */
    public Instruction(int cost, Duration time, String output)
    {
        this.cost = cost;
        this.time = time;
        this.output = output;
        this.type = PartyFactory.PartyType.ANY;
    }
    
    /**
     * Gets cost of instruction;
     * @return Cost of instruction
     */
    public int getCost()
    {
        return this.cost;
    }
    
    /**
     * Gets type of party which can handle instruction
     * @return Type of party which can handle instruction
     */
    public PartyFactory.PartyType getType()
    {
        return this.type;
    }
    
    /**
     * Gets time needed to perform instruction
     * @return Time needed to perform instruction
     */
    public Duration getTime()
    {
        return this.time;
    }
    
    /**
     * Gets output of instruction
     * @return Output of instruction
     */
    public String getOutput()
    {
        return this.output;
    }
    
    /**
     * Gets name of instruction
     * @return Name of instruction
     */
    public String getName()
    {
        return "<abstract>";
    }
}
