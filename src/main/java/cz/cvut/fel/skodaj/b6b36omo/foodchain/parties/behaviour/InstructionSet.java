/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.behaviour;

import cz.cvut.fel.skodaj.b6b36omo.foodchain.parties.PartyFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing list of instructions which leads to foodstuff
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class InstructionSet {
    
    /**
     * List of instructions which leads to foodstuff
     */
    private List<Instruction> instructions;
    
    /**
     * Index of last done instruction
     */
    private int last = 0;
    
    /**
     * Create new list of instructions which leads to foodstuff
     */
    public InstructionSet()
    {
        this.instructions = new ArrayList<>();
    }
    
    /**
     * Adds instruction to instruction set
     * @param i Instruction which will be added
     * @return {@code TRUE} if instruction can be added,
     *          {@code FALSE} otherwise
     */
    public boolean add(Instruction i)
    {
        /*
        boolean reti = false;
        if (this.instructions.size() + 1 < PartyFactory.partyOrder.length)
        {
            if (i.getType() == PartyFactory.partyOrder[this.instructions.size()])
            {
                reti = true;
                this.instructions.add(i);
            }
        }
        return reti;
        */
        this.instructions.add(i);
        return true;
        
    }
    
    /**
     * Checks, whether there is some remaining instruction
     * @return {@code TRUE} if there is some unfinished instruction,
     *         {@code FALSE} otherwise
     */
    public boolean hasNext()
    {
        boolean reti = true;
        if (this.last == this.instructions.size())
        {
            reti = false;
        }
        return reti;
    }
    
    /**
     * Get next instruction which should be done
     * @return Instruction which should be done
     */
    public Instruction next()
    {
        Instruction reti = null;
        if (this.hasNext())
        {
            reti = this.instructions.get(this.last);
            this.last++;
        }
        return reti;
    }
    
    /**
     * Gets last performed instruction
     * @return Last performed instruction
     */
    public Instruction getLastInstruction()
    {
        return this.instructions.get(this.last - 1);
    }
    
}
